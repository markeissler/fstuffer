/******************************************************************************
 *
 * This file is part of fstuffer.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           cldbmysql.h
 *
 *  Description:    Contains all functionality for interaction with a mySQL
 *                  database 
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          
 *  
 *****************************************************************************/
#include <mysql.h>
#include "common.h"
#include "clcomopts.h"
#include "clfitem.h"

#ifndef __CL_DBMYSQL__
#define __CL_DBMYSQL__

extern const USHORT k_MAX_mysql_QUERY_LEN;

class clDBMysql {
    public:
        clDBMysql(const clComOpts &r_myComOpts);
        ~clDBMysql();
    
        void init(const clComOpts &r_myComOpts);
        void set(const clComOpts &r_myComOpts); // setup our private vars
        char *setDBHost(const char * const myDBHost);
        char *setDBPort(const char * const myDBPort);
        char *setDBName(const char * const myDBName);
        char *setDBTName(const char * const myDBTName);
        char *setDBUser(const char * const myDBUser);
        char *setDBPass(const char * const myDBPass);
        char *getDBHost() const { return(dbHost); }
        char *getDBPort() const { return(dbPort); }
        char *getDBName() const { return(dbName); }
        char *getDBTName() const { return(tblName); }
        char *getDBUser() const { return(user); }
        char *getDBPass() const { return(pass); }
        bool dbinit();
        bool dbopen();
        void dbclose();
        bool dbcreateTable();
        bool dbdropTable();
        bool dbpromptDropTable(FILE *istream, FILE *ostream);
        bool dbinsert(const clFItem &r_myFItem);
        bool dbinsert_link(const clFItemNode &r_myFItemNodeHead);
        bool dbquery_link(clFItemNode **h_myFItemNodeHead);
        bool dbrepair(USHORT dbErrCode);
    
    private:
        char *dbHost;   // database host name or ip
        char *dbPort;   // database host port number
        char *dbName;   // database name
        char *tblName;  // table name
        char *user;     // user name
        char *pass;     // password
    
        MYSQL *conn;    // pointer to connection handle
        
};

#endif
