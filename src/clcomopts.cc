/******************************************************************************
 *
 * This file is part of fstuffer.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           clcomopts.cc
 *
 *  Description:    Command Line Options parsing routines. Creates an object
 *                  that contains all of the parsed parameters and flags.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *          
 *          $Log$
 *          
 *  
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>     // for atoi()
#include "common.h"
#include "clcomopts.h"
#include <mutil/util_error.h>
#include <mutil/util_fname.h>
#include <mutil/util_string.h>

#define _GNU_SOURCE     // for getopt_long()
#include <getopt.h>     // for getopt_long()



/* clComOpts::clComOpts() CONSTRUCTOR
 *
 * Des: Initialize internal variables/arrays then call init() with argc and 
 * argv which will be parsed and appropriate values copied into the private
 * data areas contained within the object.
 *
 * Rec: -int argc
 *      -ptr to argv[]
 *
 * Ret: Nothing.
 *
 * Chg: Set various values in our private data.
 *
 */
// constructors
clComOpts::clComOpts(int argc, char *argv[]):
    dbHost(0),
    dbPort(0),
    dbName(0),
    tblName(0),
    user(0),
    pass(0),
    fName(0),
    fLen(0),
    randomNum(0)
{

    init(&argc, argv);
    
    return; 
}

/* clComOpts::~clComOpts() DESTRUCTOR
 *
 * Des: --
 *
 * Rec: Nothing.
 *
 * Ret: Nothing.
 *
 * Chg: Free allocated memory where necessary.
 *
 */
 clComOpts::~clComOpts() {
    delete dbHost;
    dbHost = 0;
    delete dbPort;
    dbPort = 0;
    delete tblName;
    tblName = 0;
    delete user;
    user = 0;
    delete pass;
    pass = 0;
    delete fName;
    fName = 0;
    delete fLen;
    fLen = 0;
 }
 

/* clComOpts::init()
 *
 * Des: Set initial values for various flags contained in our private data
 * area. Cycle through the argv by calling getopt() repeatedly, setting
 * our flags and char arrays as appropriate. Then perform some error checks
 * (i.e. validate) the command line arguments passed before returning. We
 * set the badargs flag if we encounter problems with the command line
 * logic.
 *
 * Rec: -ptr to argc
 *      -ptr to argv[]
 *
 * Ret: Nothing.
 *
 * Chg: Set various values in our private data.
 *
 */
void clComOpts::init(int *argc, char *argv[]) {
    char c;
    extern char *optarg;
    
    /*
     * Define the array of option structs for use with 
     * our calls to getopt_long().
     *
     * Note: We only support --help, --version, and
     * --verbose for long options (with this version).
     *
     */
    static struct option const longopts[] = {
        {"help", no_argument, 0, 'h'},
        {"verbose", no_argument, 0, 'v'},
        {"version", no_argument, 0, 'V'},
        {0, 0, 0, 0}
    };

    
    // set dump flag to false
    setDump();
    
    // set query flag to false
    setQuery();
    
    // set badargs flag to false
    setBad();
    
    // set help flag to false
    setHelp();
    
    // set release flag to false
    setVersion();
    
    // set verbose flag to false
    setVerbose();
    
    // set random flag to false
    setRandom();
    
    // set erase/drop flag to false
    setDrop();
    
    // get command line options by repeatedly calling getopt_long()
    while((c = getopt_long(*argc, argv, "f:d:t:u:p:r:s:hvH:P:DEQVR", longopts, NULL)) > 0) {
        switch(c) {
            case 'f':
                str_copyStr(&fName, optarg);
                break;
                
            case 'd':
                str_copyStr(&dbName, optarg);
                break;
                
            case 'h':
                setHelp(true);
                break;
            
            case 't':
                str_copyStr(&tblName, optarg);
                break;
                
            case 'u':
                str_copyStr(&user, optarg);
                break;
                
            case 'p':
                str_copyStr(&pass, optarg);
                break;
                
            case 'H':
                str_copyStr(&dbHost, optarg);
                break;
                
            case 'P':
                str_copyStr(&dbPort, optarg);
                break;
                
            case 'r':
                if(atoi(optarg) < 0 || atoi(optarg) > 65535)
                    setBad(true);
                else
                    setRandomNum(atoi(optarg));
                break;              
                
            case 's':
                str_copyStr(&fLen, optarg);
                break;
                
            case 'v':
                setVerbose(true);
                break;
                
            case 'D':
                setDump(true);
                break;
                
            case 'E':
                setDrop(true);
                break;
                
            case 'Q':
                setQuery(true);
                break;
                
            case 'V':
                setVersion(true);
                break;
                
            case 'R':
                setRandom(true);
                break;
                
            default:
                setBad(true);
                setHelp(true);  // call usage() for bad args
                break;
                
        }

    } // end of while

    /*
     * If Query (-Q) and Erase/Drop (-E) are both on, we 
     * have an error condition.
     *
     */
    if(getQuery() == true && getDrop() == true)
        setBad(true);   // error    
    
    /*
     * If Query (-Q) is on, we need a table name; if off,
     * we need a filename. You cannot specify both -D and -Q
     * options as we can't dump a file and a table at the
     * same time...or we wouldn't want to.
     *
     * This version doesn't support injecting random items
     * into the database, nor does it support extracting
     * random items from a file. So if random is specified
     * while -Q is off, then we will have to abort!
     *
     */
    if(getQuery() == true || getDrop() == true) {
        // We cannot dump from a database! Only from a file!
        // We cannot drop and dump at the same time!
        if(getDump() == true) {
            setBad(true); // error!
        }
         
        // If no tblName, then we need a filename to base the
        // tblName on!
        if(tblName == 0 && fName == 0) {
            printf("I need a table name!!!\n");
            setBad(true); // error!
        }
    } else {
        // We're not pulling data from the database, so we need
        // a filename to pull from! Make sure we're not looking
        // for version info.
        if(fName == 0 && (getVersion() == false && getHelp() == false)) {
            printf("I need a file name!!!\n");
            setBad(true); // error!
        }
        
        // We can't inject random items into the database!
        if(fName != 0 && (getRandom() == true || getRandomNum() != 0)) {
            printf("Can't inject a random item into database!!!\n");
            setBad(true); // error!
        }
    }
    
    /*
     * If we've gotten this far, it's possible that only the 
     * -v or --version flags have been set and that the user
     * wants only version info. On the other hand, if we got
     * this far and version has been requested along with
     * other flags...we need to setBad(true)!
     *
     */
    if(*argc > 2 && (getVersion() == true || getHelp() == true)) {
        setBad(true); // error!
    }
    
    
    /*
     * set defaults
     *
     */
    if(fName == 0)
        str_copyStr(&fName, "");
        
    if(dbName == 0)
        str_copyStr(&dbName, kDefDName);
        
    if(tblName == 0 && fName != 0) {
        const char *fbasename = fnameBase(fName, 1);

        str_copyStr(&tblName, fbasename);
        
        delete(fbasename);
    } else if (tblName == 0) 
        str_copyStr(&tblName, "");
        
    if(user == 0)
        str_copyStr(&user, kDefUser);
        
    if(pass == 0)
        str_copyStr(&pass, kDefPass);
        
    if(dbHost == 0)
        str_copyStr(&dbHost, kDefHost);

    if(dbPort == 0)
        str_copyStr(&dbPort, kDefPort);

    if(fLen == 0)
        str_copyStr(&fLen, kDefFLen);
    
    return;
}

