/******************************************************************************
 *
 * This file is part of fstuffer.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           clffile.cc
 *
 *  Description:    Contains required functionality to read and parse a 
 *                  fortune(1) format file and then stuff each fortune
 *                  item into a linked list.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.2  2000/11/11 00:50:54  mark
 *          Changed a mem check routine in parse_link()
 *
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include <stdio.h>
#include <fstream>
#include <limits.h>
#include <iomanip.h>
#include "common.h"
#include <mutil/util_error.h>
#include <mutil/util_string.h>
#include "clffile.h"
#include "clfitem.h"

/* clFFile::clFFile() CONSTRUCTOR
 *
 * Des: Calls init() with a ptr to char array holding path to the file
 * to read from and passes a maximum file length of 0 (unlimited).
 *
 * Rec: -ptr to char array containing path to input file
 *
 * Ret: Nothing.
 *
 * Chg: Nothing.
 *
 */
clFFile::clFFile(const char *inFile) {

    init(inFile, 0);

    return;
}


/* clFFile::clFFile() CONSTRUCTOR
 *
 * Des: Calls init() with a ptr to a char array holding path to the file
 * to read from and a USHORT indicating maximum item length in bytes.
 *
 * Rec: -ptr to char array containing path to input file
 *      -USHORT indicating max item length in bytes
 *
 * Ret: Nothing.
 *
 * Chg: Nothing.
 *
 */
clFFile::clFFile(const char *inFile, USHORT maxItemLen) {

    init(inFile, maxItemLen);
    
    return;
}


/* clFFile::init()
 *
 * Des: Sets private data values fName (file name) char array and USHORT
 * fMaxItemLen to values passed. 
 *
 * Rec: -ptr to char array containing path to input file
 *      -USHORT indicating max item length in bytes
 *
 * Ret: Nothing.
 *
 * Chg: Set various values in our private data.
 *
 */
void clFFile::init(const char *inFile, USHORT maxItemLen) {
    
    str_copyStr(&fName, inFile);
    fMaxItemLen = maxItemLen;

}


/* clFFile::show()
 *
 * Des: Opens file at path indicated by fName and simply reads stream and
 * outputs data (to STDOUT) until EOF is reached. It's a simple dump.
 *
 * Rec: Nothing.
 *
 * Ret: -bool indicating success (false) or failure (true).
 *
 * Chg: Nothing.
 *
 */
bool clFFile::show() {
    char ch;
    bool myErr = false;

    fin.open(fName);
    
    if(fin.fail()) {
        myErr = true;
        cout << ">>>> ACK!\n";
    } else {
    
        while(fin.good()) {
            fin.get(ch);
            cout << ch; 
        }
        
        cout << ">>>> EOF" << endl;
    }
    
    fin.close();

    return(myErr);
}


/* clFFile::parse()
 *
 * Des: Reads the fortune file specified on the command line and performs
 * a simply parse on each item. That is, we simply detect the '%' which 
 * designates the end of an item and write the item to stdout with a size
 * summary. This is used in conjunction with the -D flag.
 *
 * Rec: Nothing.
 *
 * Ret: -bool indicating success (false) or failure (true).
 *
 * Chg: Nothing.
 *
 */
bool clFFile::parse() {
    char *myBuffer = new char[kBufSize];
    USHORT myBufPtr = 0;
    int done = 0, entries = 0;
    bool myErr = false;


    if(myBuffer == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clFFile::parse", __LINE__, k_ue_memAlloc);
            
    fin.open(fName);

    if(!fin.good()) {
        myErr = true;
        return(myErr);
    }
    
    do {
        while(myBufPtr < (kBufSize - 1) && (done == 0)) {
            fin.get(myBuffer[myBufPtr]);
        
            /* We will set done to -1 if we hit end of file AFTER
             * finding a "%" sign. This means there's extraneous
             * data after the last record. We need to ignore that
             * junk and bail.
             */
            if(fin.eof())
                done = -1;
                
            /* Item end is defined by a '%' followed by a '\n', we
             * use peek() to see if the next char in the stream is
             * a '\n' so that we can set done = 1 correctly.
             */
            if(myBuffer[myBufPtr] == '%' && fin.peek() == '\n')
                done = 1;
            else
                myBufPtr++;
        }

        if(done > 0) {
            myBuffer[myBufPtr] = '\0';
        
            if(fMaxItemLen == 0 || (fMaxItemLen > 0 && myBufPtr <= fMaxItemLen))
                cout << myBuffer << "[size=" << myBufPtr << "]" << endl;
                    
            done = 0;
            myBufPtr = 0;
            
            entries++;
        } else {
            break;
        }

    } while(!fin.eof());
        
    fin.close();
    
    delete myBuffer;
    
    /*
     * Report finding of extraneous data.
     */
    if(done < 0)
        cout << ">>>> ERROR buffer overrun! Extraneous data ignored." << endl;

    return(myErr);
} 


/* parse_link()
 *
 * Des: Reads the fortune file specified on the command line and executes
 * a parsing routine to extract each item, then creates a new clFItem which
 * is initalized with a buffer length, and a buffer char array, and will 
 * further parse the item into body, author, and reference parts. We then
 * stuff a ptr to the resulting object into a clFItemNode and then insert
 * the node into a linked list of clFItemNodes.
 *
 * This function is meant to be called when inserting data into a database
 * or whenever else we need to create a linked list of items to facilitate
 * further processing.
 *
 * Rec: Nothing.
 *
 * Ret: Nothing.
 *
 * Chg: Allocates a clFItem and clFItemNode for each parsed item before
 * extending a linked list of clFItemNode objects.
 *
 */
clFItemNode *clFFile::parse_link() {
    char *myBuffer = new char[kBufSize];
    USHORT myBufPtr = 0;
    int entries = 0;
    int done = 0;
    clFItemNode *listHead = 0;
    clFItemNode *newNode = 0;
    clFItem *newclFItem = 0;
        
    if(myBuffer == 0) {
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clFFile::parse_link", __LINE__, 
                k_ue_memAlloc);
    }
    
    fin.open(fName);

    /* If we have an error now, we will return listHead which is
     * currently empty. That satisfies our error condition.
     */
    if(!fin.good()) 
        return(listHead);
            
    do {
        while(myBufPtr < (kBufSize - 1) && (done == 0)) {
            fin.get(myBuffer[myBufPtr]);
            
            /* We will set done to -1 if we hit end of file AFTER
             * finding a "%" sign. This means there's extraneous
             * data after the last record. We need to ignore that
             * junk and bail.
             */
            if(fin.eof())
                done = -1;

            /* Item end is defined by a '%' followed by a '\n', we
             * use peek() to see if the next char in the stream is
             * a '\n' so that we can set done = 1 correctly.
             */
            if(myBuffer[myBufPtr] == '%' && fin.peek() == '\n') {
                done = 1;
                fin.ignore(1,'\n'); // ignore the new line
            } else
                myBufPtr++;
        }


        if(done > 0) {
            myBuffer[myBufPtr] = '\0';
            
            // copy data to a clFItemNode in linked list
            newclFItem = new clFItem(myBufPtr, myBuffer);
            if(newclFItem == 0)
                errorMsg(gpOut, k_ue_fatal, __FILE__, "clFFile::parse_link", __LINE__, 
                        k_ue_memAlloc);

            if (entries == 0) {
                listHead = new clFItemNode(newclFItem);
                if(listHead == 0)
                    errorMsg(gpOut, k_ue_fatal, __FILE__, "clFFile::parse_link", __LINE__, 
                            k_ue_memAlloc);
            } else {
                newNode = new clFItemNode(newclFItem);
                if(newNode == 0)
                    errorMsg(gpOut, k_ue_fatal, __FILE__, "clFFile::parse_link", __LINE__, 
                            k_ue_memAlloc);
                
                listHead->insertNode(newNode);
                
                // increment item counter
                listHead->setListLen(listHead->getListLen() + 1);           
            } 
                            
            done = 0;
            myBufPtr = 0;
    
            entries++;
        } else {
            break;
        }
                    
    } while(!fin.eof());
    
    fin.close();
    
    delete myBuffer;
    
    /*
     * Report finding of extraneous data.
     */
    if(done < 0)
        cout << ">>>> ERROR buffer overrun! Extraneous data ignored." << endl;

    return(listHead);
} 
