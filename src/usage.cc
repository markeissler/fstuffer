/******************************************************************************
 *
 * This file is part of fstuffer.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           usage.cc
 *
 *  Description:    Contains functionality to output basic usage info.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *          
 *          $Log$
 *          Revision 1.2  2000/11/13 19:24:49  mark
 *          Updated to support new error messages with debug code
 *
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "clcomopts.h"


/* usage()
 *
 * Des: Prints usage message then returns.
 *
 * Rec: Nothing.
 *
 * Ret: Nothing.
 *
 * Chg: Nothing.
 *
 */
void usage(const clComOpts &myOpts) {
    
    // Overview
    fprintf(gpOut, "Usage: %s -f filename [-D | -E | -Q] [OPTION]\n", kDefPackage);
    fprintf(gpOut, "\nExample: %s -f /usr/share/games/fortunes/wisdom\n", kDefPackage);
    fprintf(gpOut, "  -f               filename [%s]\n", myOpts.getFName());
    fprintf(gpOut, "  -D               dump mode, prints fortune data to STDOUT\n");
    fprintf(gpOut, "  -E               erase (drop) table from database\n");
    fprintf(gpOut, "  -Q               query mode, pulls fortune data from database\n");

    // General Options
    fprintf(gpOut, "\nGeneral Options:\n");
    fprintf(gpOut, "  -R               random mode, a random fortune will be returned\n");
    fprintf(gpOut, "  -r #             random mode, with random item specified by user\n");
    
    fprintf(gpOut, "  -s               maximum item length ");
    if(!strcmp(kDefFLen, "0"))
        fprintf(gpOut, "[%d bytes]\n", kBufSize);
    else
        fprintf(gpOut, "[%s]\n", myOpts.getFLen());

    fprintf(gpOut, "  -v, --verbose    set verbosity on\n");
    fprintf(gpOut, "\nNOTE 1: Specified file to read from must be in fortune(1) format.\n");
    fprintf(gpOut, "\nNOTE 2: Either a table name or a file name must be specified when\n");
    fprintf(gpOut, "using the Query mode of operation.\n");

    // DB Options
    fprintf(gpOut, "\nDatabase Options:\n");
    fprintf(gpOut, "  -d               DB name [%s]\n", myOpts.getDName());
    fprintf(gpOut, "  -t               DB table name [%s]\n", myOpts.getTName());
    fprintf(gpOut, "  -u               DB user name [%s]\n", myOpts.getUser());
    fprintf(gpOut, "  -p               DB user password [%s]\n", myOpts.getPass());

    // DB Server Options
    fprintf(gpOut, "\nDatabase Server Options:\n");
    fprintf(gpOut, "  -H               DB server hostname or IP address [%s]\n", myOpts.getHost());
    fprintf(gpOut, "  -P               DB server port number [%s]\n", myOpts.getPort());

    // Miscellaneous
    fprintf(gpOut, "\nMiscellaneous:\n");
    fprintf(gpOut, "  -h, --help       display this help and exit\n");
    fprintf(gpOut, "  -V, --version    print version information and exit\n");
    
    // Email bugs
    fprintf(gpOut, "\nReport bugs to <%s>.\n", kDefDebugEmail);

    return;
}

