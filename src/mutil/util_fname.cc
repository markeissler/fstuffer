/******************************************************************************
 *
 * This file is part of mixtur libmutil.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           util_fname.cc
 *
 *  Description:    Contains functionality for generic file name manipulation.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          
 *  
 *****************************************************************************/
#include "util_fname.h"
#include "util_string.h"
 
 /* fnameBase()
 *
 * Des: Chop of all leading path information from a filename in order
 * to extract the base name (meaning, just the filename). 
 *
 * NOTE: You must call delete() on the returned string when done as we
 * allocate memory.
 *
 * Rec: -ptr to char array containing string to manipulate.
 *
 * Ret: -ptr to new array containing truncated name.
 *
 * Chg: Nothing.
 *
 */
char *fnameBase(const char *fname, int bTrunc) {
    const char *p = fname;
    char *baseName = 0;
    
    int i;
    
    for(i = 0; *(fname + i); i++) {
    
        // If we find a slash while iterating through the fname string,
        // we must reset baseName to beyond the slash.
        if(*(fname + i) == '/')
            p = fname + i + 1;
    }

    if(bTrunc)
        baseName = fnameTrunc(p);       // allocates memory!
    else
        str_copyStr(&baseName, p);      // allocates memory!
    
    return(baseName);
    
} // end of fnameBase()


/* fnameTrunc()
 *
 * Des: Chops a filename extension off of a filename. The separator is
 * a "." and we simply cut from there.
 *
 * NOTE: You must call delete() on the returned string when done as we
 * allocate memory in this routine.
 *
 * Rec: -ptr to char array containing string to manipulate.
 *
 * Ret: -ptr to new array containing truncated name.
 *
 * Chg: Nothing.
 *
 */
char *fnameTrunc(const char *fname) {
    char *truncName = 0;
    char *fnameCopy = 0;
    
    int i, dotfound;
    
    // make working copy of fname
    str_copyStr(&fnameCopy, fname);
    
    for(i = 0, dotfound = 0; !dotfound && *(fnameCopy + i); i++) {

        // look for a dot
        if(*(fnameCopy + i)     == '.') {
            *(fnameCopy + i) = '\0';
            dotfound = 1;
        }
        
    }
    
    str_copyStr(&truncName, fnameCopy); // allocates memory!
    
    // cleanup
    delete fnameCopy;

    return(truncName);
} // end of fnameTrunc()

