/******************************************************************************
 *
 * This file is part of mixtur libmutil.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           util_string.h
 *
 *  Description:    Contains functionality for generic string manipulation.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#ifndef __bUTIL_STRING__
#define __bUTIL_STRING__


/*
 * STRUCTS
 */
 
// escape string type
typedef struct {
    char *str;
    unsigned short int len;
} eString;

/*
 * PROTOS
 */
char *str_copyStr(char **oldString, const char *const newString);
char *str_shrinkStr(char **inString);
char *str_compressStr(char **inString);
char *str_upperStr(char *inString);
char *str_lowerStr(char *inString);
eString *str_eStr(const char * const inStr, unsigned short int inStrLen);
void free_eStr(eString *ePtr);

#endif

