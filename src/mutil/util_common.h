/******************************************************************************
 *
 * This file is part of mixtur libmutil.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           util_common.h
 *
 *  Description:    Contains references to common include files and declared
 *                  contstant variables for general inclusion.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          
 *  
 *****************************************************************************/
#ifndef _bUTIL_COMMON_
#define _bUTIL_COMMON_

// integers
typedef unsigned short USHORT;
typedef unsigned long ULONG; 

/*
 * Defined in main.c
 *
 */
extern const USHORT kUtilBufSize;
extern const USHORT kUtilBufSizeSmall;

/*
 * PACKAGE/VERSION
 *
 */
extern const char *const kDefPackage;
extern const char *const kDefVersion;

#endif
