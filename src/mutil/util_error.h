/******************************************************************************
 *
 * This file is part of mixtur libmutil.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           util_error.h
 *
 *  Description:    Contains functionality for generic error routines.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.2  2000/11/13 19:22:17  mark
 *          Added debug code in errorMsg and errorCMsg. All callers supply function name and stream for error output.
 *
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#ifndef __bUTIL_ERROR__
#define __bUTIL_ERROR__

/*
 * Defined in util_error.cc
 *
 */
extern const char *const kErrMemAlloc;
extern const char *const kErrFileRead;
extern const char *const kErrFileWrite;
extern const char *const kErrFileOther;


/*
 * Enumerated constants
 */
enum ERRTYPE { k_ue_fileRead = 10, k_ue_fileWrite, k_ue_fileOther, k_ue_memAlloc };
enum ERRLEVEL { k_ue_warn = 90, k_ue_fatal };


/*
 * PROTO
 *
 */

void errorMsg(FILE *stream, ERRLEVEL eLevel, const char *eFile, const char *eFunc, 
        int eLine, ERRTYPE eType);
void errorCMsg(FILE *stream, ERRLEVEL eLevel, const char *eFile, const char *eFunc, 
        int eLine, ...);

#endif

