/******************************************************************************
 *
 * This file is part of mixtur libmutil.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           util_prompt.cc
 *
 *  Description:    Contains functionality for creating console prompts.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include <stdio.h>
#include <stdarg.h>     // for variable length arg lists

#include <string.h>     // for strncpy, strcmp
#include "util_common.h"
#include "util_error.h"
#include "util_string.h"


/* promptCList()
 *
 * Des: Outputs a supplied question message and prompts user to input one
 * of the responses supplied in a variable length argument list. This routine
 * will keep prompting until one of the supplied responses has been entered.
 * The routine will return the first character of the response so you can
 * easily use the return value for switch statements, etc. The full response
 * string will be stored in aStr (answer string).
 * 
 * NOTE!! This is a case insensitve routine. All lowercase responses in our
 * arg list will be converted to upper case after being copied to an array.
 * All lowercase responess from user input will also be converted to upper-
 * case prior to matching. This simplifies the routine.
 *
 * NOTE!! delete() must be called on the aStr after use as we will allocate
 * memory to that ptr.
 *
 * Rcv: -ptr to FILE struct for input stream (to receive prompt response)
 *      -ptr to FILE struct for output stream (to ouput prompt)
 *      -ptr to char array containing question
 *      -USHORT indicating number of answers to follow
 *      -one or more char arrays (strings) containing possible answers.
 *
 * NOTE!! The list of strings must be ended with a zero length
 * string to signify end of list.
 *
 * Ret: -first char in response array
 *
 * Chg: Allocates memory to aStr after deleting original allocation. Then
 * copies response string into that space.
 */
char promptCList(FILE *istream, FILE *ostream, char **aStr, 
        const char *qStr, USHORT aListLen, ...) {
    FILE *stream = stdout;
    int i;
    bool done = false;
    
    char *s;
    va_list ap;
    
    char *aStrList[aListLen];

    // create a temporary aStr to receive user input
    char *tempaStr = new char[kUtilBufSizeSmall];
    if(tempaStr == 0)
        errorMsg(ostream, k_ue_fatal, __FILE__, "promptCList", __LINE__, k_ue_memAlloc);
    *tempaStr = '\0';   // copy a null so we return a valid char array
        
    // initialize ap
    va_start(ap, aListLen);

    /*
     * Process arg list and stuff each item into
     * the aStrList after allocating memory.
     *
     * NOTE: We convert each item into uppercase
     * before stuffing.
     *
     */
    for(i = 0; strlen(s = va_arg(ap, char *)) != 0 && i < aListLen; i++) {
        aStrList[i] = new char[strlen(s) + 1];
        if(aStrList[i] == 0)
            errorMsg(ostream, k_ue_fatal, __FILE__, "promptCList", __LINE__, k_ue_memAlloc);
        strncpy(aStrList[i], s, strlen(s) + 1);
        str_upperStr(aStrList[i]);  // convert to upper case
    }

    // clean up va_arg stuff
    va_end(ap);

    /*
     * Show question string (qStr) and prompt for response until
     * user enters a response in our argument list.
     *
     */
    do {
        fprintf(ostream, "%s", qStr);   // print question
        fgets(tempaStr, kUtilBufSizeSmall, istream);    // get answer
        str_upperStr(tempaStr);         // convert to upper case
        str_compressStr(&tempaStr);     // remove trailing new line

        for(i = 0; i < aListLen && done == false; i++) {
            if(strcmp(tempaStr, aStrList[i]) == 0)
                done = true;
        }
    
    } while(done == false); // done


    /*
     * Copy temporary aStr to aStr. This will resize the string.
     *
     */
    str_copyStr(aStr, tempaStr);
        

    /*
     * Free memory just allocated above.
     *
     */
    for(i = 0; i < aListLen; i++) {
        delete(aStrList[i]);
        aStrList[i] = 0;
    }
    delete(tempaStr);

    return(*aStr[0]);
}
