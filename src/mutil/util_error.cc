/******************************************************************************
 *
 * This file is part of mixtur libmutil.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           util_error.cc
 *
 *  Description:    Contains functionality for generic error routines.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.2  2000/11/13 19:22:17  mark
 *          Added debug code in errorMsg and errorCMsg. All callers supply function name and stream for error output.
 *
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h> // for variable length arg lists
#include "util_common.h"
#include "util_error.h"
#include "util_fname.h"

/*
 * constants
 *
 */
const char *const kErrMemAlloc = "Couldn't allocate memory!";
const char *const kErrFileRead = "File read error!";
const char *const kErrFileWrite = "File write error!";
const char *const kErrFileOther = "File r/w error unknown";


/* errorMsg()
 *
 * Des: Outputs an error message appropriate for the ERRTYPE
 * and ERRLEVEL specified by the caller.
 *
 * Rcv: -ptr to FILE stream for output
 *      -ERRLEVEL
 *      -ptr to char arraay containing source code file name
 *      -ptr to char array contianing function name
 *      -int specifying line number
 *      -ERRTYPE
 *
 * Ret: Nothing.
 *
 * Chg: Nothing.
 */
 void errorMsg(FILE *stream, ERRLEVEL eLevel, const char *eFile, const char *eFunc, 
            int eLine, ERRTYPE eType) {
            
    char errStr[kUtilBufSizeSmall];
    
    // determine which error to print based on eType
    switch(eType) {
        case k_ue_fileRead:
            snprintf(errStr, kUtilBufSizeSmall, "%s", kErrFileRead);
            break;
            
        case k_ue_fileWrite:
            snprintf(errStr, kUtilBufSizeSmall, "%s", kErrFileWrite);
            break;
            
        case k_ue_fileOther:
            snprintf(errStr, kUtilBufSizeSmall, "%s", kErrFileOther);
            break;
        
        case k_ue_memAlloc:
            snprintf(errStr, kUtilBufSizeSmall, "%s", kErrMemAlloc);
            break;
    }

#ifdef DEBUG
    /*
     * In debug mode, we print line number, filename (with extension) and function
     * name for where the error occurred.
     *
     */
    const char *fbasename;

    fbasename = fnameBase(eFile, 1);    // strip path and .ext from filename
   
    fprintf(stream, "(#%.4d - %s, %s) %s\n", eLine, fbasename, eFunc, errStr);
    
    // clean up
    delete(fbasename);

#else
    /*
     * Print the line number and function name where the error occurred.
     *
     */
    fprintf(stream, "(#%.4d - %s) %s\n", eLine, eFunc, errStr);
#endif


    // determine exit action based on eLevel
    switch(eLevel) {
        case k_ue_warn:
            ;
            break;
            
        case k_ue_fatal:
            exit(1);
            break;
            
    }
    
    return;
}

/* errorCMsg()
 *
 * Des: Outputs a custom error message whose string(s) are 
 * passed to us by the caller.
 *
 * Rcv: -ptr to FILE stream for output
 *      -ERRLEVEL
 *      -ptr to char arraay containing source code file name
 *      -ptr to char array contianing function name
 *      -int specifying line number
 *      -ptr(s) to additional char arrays containing custom error messages
 *
 * NOTE!! The list of strings must be ended with a zero length
 * string to signify end of list.
 *
 * Ret: Nothing.
 *
 * Chg: Nothing.
 */
void errorCMsg(FILE *stream, ERRLEVEL eLevel, const char *eFile, const char *eFunc, 
        int eLine, ...) {
    
    char *s;
    
    va_list ap;
    
    va_start(ap, eLine);
    
    
#ifdef DEBUG
    /*
     * In debug mode, we print line number, filename (with extension) and function
     * name for where the error occurred.
     *
     */
    const char *fbasename;
     
    fbasename = fnameBase(eFile, 0);    // strip path only from filename

    // process arg list
    while(strlen(s = va_arg(ap, char *)) != 0){

        fprintf(stream, "(#%.4d - %s, %s) %s\n", eLine, fbasename, eFunc, s);
        
    }
    
    // clean up
    delete(fbasename);
    
#else
    /*
     * Print the line number and function name where the error occurred.
     *
     */

    // process arg list
    while(strlen(s = va_arg(ap, char *)) != 0){

        fprintf(stream, "(#%.4d - %s) %s\n", eLine, eFunc, s);
        
    }
#endif
    
    // clean up
    va_end(ap);
    
    // determine exit action based on eLevel
    switch(eLevel) {
            case k_ue_warn:
            ;
            break;
            
        case k_ue_fatal:
            exit(1);
            break;
            
    }

    return;
}

