/******************************************************************************
 *
 * This file is part of mixtur libmutil.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           util_string.cc
 *
 *  Description:    Contains functionality for generic string manipulation.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include <stdio.h>
#include <string.h> // for memmove()
#include <ctype.h>  // for toupper() and tolower()
#include "util_string.h"
#include "util_error.h"


/* str_copyStr()
 *
 * Des: Delete memory block allocated at *oldString. Allocate memory for the
 * newString, then make a copy of that string to new memory block. Finally,
 * copy ptr to that new block into the *oldString ptr variable.
 *
 * Rec: -handle to char array holding old string.
 *      -ptr to char array holding new string.
 *
 * Ret: -ptr to char array holding old string.
 *
 * Chg: Free memory allocated to oldString and pointed to by ptr variable
 * at *oldString, allocate new memory to hold copy of newString and then 
 * store address of first byte in that block in the ptr variable which 
 * previously pointed to oldString.
 *
 */
char *str_copyStr(char **oldString, const char *const newString) {
    if(*oldString != 0)
        delete *oldString;
    
    *oldString = new char[strlen(newString) + 1];
    if(*oldString == 0)
        errorMsg(stdout, k_ue_fatal, __FILE__, "str_copyStr", __LINE__, k_ue_memAlloc);
    strncpy(*oldString, newString, strlen(newString) + 1);
        
    return(*oldString);
}


/* str_shrinkStr()
 *
 * Des: Removes all spaces in a string. 
 *
 * Rec: -ptr to ptr (i.e. handle) to char array containing string
 * to examin and modify.
 *
 * Ret: -ptr to input string.
 *
 * Chg: Changes data in char array passed to us by reference.
 *
 */
char *str_shrinkStr(char **inString) {
    char *p, *tempStr;

    for(p = *inString; *p && *p == ' '; p++) {
        ;
    }
        
    tempStr = new char[strlen(p) + 1];
    if(tempStr == 0)
        errorMsg(stdout, k_ue_fatal, __FILE__, "str_shrinkStr", __LINE__, k_ue_memAlloc);    
    
    strcpy(tempStr, p);

    if(*inString != 0)
        delete (*inString);
    
    *inString = new char[strlen(tempStr) + 1];
    if(*inString == 0)
        errorMsg(stdout, k_ue_fatal, __FILE__, "str_shrinkStr",__LINE__, k_ue_memAlloc);    
    
    strcpy(*inString, tempStr);
    
    delete tempStr;

    return(*inString);
}


/* str_compressStr()
 *
 * Des: Removes the following from inString:
 *          "\n ", "\t ", "   " (three spaces)
 *          
 *      Replaces the following with a single " ":
 *          "\n", "\t"
 *
 * We rewrite the modified string to the input array.
 *
 * Rec: -ptr to ptr (i.e. handle) to char array containing string
 * to examine and modify.
 *
 * Ret: -ptr to input string.
 *
 * Chg: Changes data in char array passed to us by reference.
 *
 */
char *str_compressStr(char **inString) {
    char *p, *tempStr;
    int i = 0;

    tempStr = new char[strlen(*inString) + 1];
    if(tempStr == 0)
        errorMsg(stdout, k_ue_fatal, __FILE__, "str_compressStr", __LINE__, k_ue_memAlloc);

    for(p = *inString; *p; p++) {
        if(*p == '\n' || *p == '\t' && *(p + 1) == ' ') {
            ;
        } else if (*p == '\n' || *p == '\t') {
            *(tempStr + i) = ' ';
            i++;            
        } else if(*p == ' ' && (*(p - 1) == ' ' || *(p + 1) == ' ')) {
            ;
        } else {
            *(tempStr + i) = *p;
            i++;
        }
    }
    
    *(tempStr + i) = '\0';
        
            
    if(*inString != 0)
        delete (*inString);
    
    *inString = new char[strlen(tempStr) + 1];
    if(*inString == 0)
        errorMsg(stdout, k_ue_fatal, __FILE__, "str_compressStr", __LINE__, k_ue_memAlloc);

    strcpy(*inString, tempStr);
    
    delete tempStr;

    return(*inString);
}


/* str_upperStr()
 *
 * Des: Converts all letters in inString to upper case.
 *
 * Rec: -ptr to char array containing input string.
 *
 * Ret: -ptr to char array containing input string.
 *
 * Chg: Converts all letters in inString to upper case.
 *
 */
char *str_upperStr(char *inString) {
    char *p;
    
    for(p = inString; *p; p++) {
        *p = toupper(*p);
    }
    
    return(inString);
}


/* str_lowerStr()
 *
 * Des: Converts all letters in inString to lower case.
 *
 * Rec: -ptr to char array containing input string.
 *
 * Ret: -ptr to char array containing input string.
 *
 * Chg: Converts all letters in inString to lower case.
 *
 */
char *str_lowerStr(char *inString) {
    char *p;
    
    for(p = inString; *p; p++) {
        *p = tolower(*p);
    }
    
    return(inString);
}


/* str_eStr()
 *
 * Des: Searches through a char array and escapes all dangerous characters
 * like "'", newlines and so on for further inclusion in strings. The 
 * resulting string is copied to an eString struct whose memory is allocated
 * within this fucnction and a pointer is returned to the caller. The length
 * of the resulting string is also contained in the eString struct.
 *
 * Rcv: Ptr to char array of input string, unsigned short int indicating the
 * length of the input string which cannot necessarily be checked via the
 * strlen() function as the array may contain NULLs.
 *
 * Ret: Ptr to eString struct. Returns NULL on failure.
 *
 * Changes: Nothing
 *
 */
eString *str_eStr(const char * const inStr, unsigned short int inStrLen) {
    char *tempStr, *tempStr_start;
    int i;
    eString *ePtr;
    
    tempStr = new char[(inStrLen * 2) + 1];
    if(tempStr == 0)
        return(0);
        
    tempStr_start = tempStr;

    ePtr = new eString;
    if(ePtr == 0)
        return(0);
    
    for(i = 0; i < inStrLen; i++) {
        switch(*(inStr + i)) {
            case 0:
                *tempStr++ = '\\';
                *tempStr++ = '0';
                break;
                
            case '\n':
                *tempStr++ = '\\';
                *tempStr++ = 'n';
                break;
                
            case '\r':
                *tempStr++ = '\\';
                *tempStr++ = 'r';
                break;
                
            case '\\':
                *tempStr++= '\\';
                *tempStr++= '\\';
                break;

            case '\'':
                *tempStr++= '\\';
                *tempStr++= '\'';
                break;

            case '"': 
                *tempStr++= '\\';
                *tempStr++= '"';
                break;

            case '\032':        // ctrl-z
                *tempStr++= '\\';
                *tempStr++= 'Z';
                break;
            
            default:
                *tempStr++= *(inStr + i);
    
        }
    }

    *tempStr = '\0';
    
    // copy tempStr to ePtr->str after allocating memory
    ePtr->len = tempStr - tempStr_start;
    ePtr->str = new char[ePtr->len + 1];
    if(ePtr->str == 0)
        return(0);
            
    memmove(ePtr->str, tempStr_start, sizeof(char) * ePtr->len);
    memmove(ePtr->str + ePtr->len, "\0", sizeof(char));
            
    delete tempStr_start;

    return(ePtr);   
}


/* free_eStr()
 * 
 * Des: Frees memory allocated to an eString struct. This function
 * should be called to dispose of eStrings.
 *
 * Rcv: Ptr to eString struct.
 *
 * Ret: Nothing.
 *
 * Changes: Frees memory occupied by an eString struct.
 */
void free_eStr(eString *ePtr) {
    if(ePtr->str != 0)
        delete ePtr->str;
        
    delete ePtr;

    return;
}
