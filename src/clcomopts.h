/******************************************************************************
 *
 * This file is part of fstuffer.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           clcomopts.h
 *
 *  Description:    Command Line Options parsing routines. Creates an object
 *                  that contains all of the parsed parameters and flags.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include "common.h"

#ifndef __CL_COMOPTS__
#define __CL_COMOPTS__

/*
 * CLASSES
 *
 */
class clComOpts {
    public:
        clComOpts(int argc, char *argv[]);
        ~clComOpts();
        
        char *getHost() const { return(dbHost); }
        char *getPort() const { return(dbPort); }
        char *getDName() const { return(dbName); }
        char *getTName() const { return(tblName); }
        char *getUser() const { return(user); }
        char *getPass() const { return(pass); }
        char *getFName() const { return(fName); }
        char *getFLen() const { return(fLen); }
        bool getDump() const { return(dumpFlag); }
        bool getQuery() const { return(queryFlag); }
        bool getBad() const { return(badFlag); }
        bool getHelp() const { return(helpFlag); }
        bool getVersion() const { return(versionFlag); }
        bool getVerbose() const { return(verboseFlag); }
        bool getRandom() const { return(randomFlag); }
        USHORT getRandomNum() const { return(randomNum); }
        bool getDrop() const { return(dropFlag); }
        void setDump(bool dFlag = false) { dumpFlag = dFlag; }
        void setQuery(bool qFlag = false) { queryFlag = qFlag; }
        void setBad(bool bFlag = false) { badFlag = bFlag; }
        void setHelp(bool hFlag = false) { helpFlag = hFlag; }
        void setVersion(bool versFlag = false) { versionFlag = versFlag; }
        void setVerbose(bool vFlag = false) { verboseFlag = vFlag; }
        void setRandom(bool rFlag = false) { randomFlag = rFlag; }
        void setRandomNum(USHORT rNum = 0) { randomNum = rNum; }
        void setDrop(bool eFlag = false) { dropFlag = eFlag; }
        void init(int *argc, char *argv[]);
                
        
    private:
        char *dbHost;   // database host name or ip
        char *dbPort;   // database host port number
        char *dbName;   // database name
        char *tblName;  // table name
        char *user;     // user name
        char *pass;     // password
    
        char *fName;    // file name
        char *fLen;     // max item length for import
                        
        bool dumpFlag;  // if true, just parse and dump
        bool queryFlag;     // if true, query specified table
        bool badFlag;   // if true, bad CLI args were passed
        bool helpFlag;  // if true, show help info then exit
        bool versionFlag;   // if true, show release/version info then exit
        bool verboseFlag;   // if true, more crap is output
        bool randomFlag;    // if true, we return 1 item
        bool dropFlag;      // if true, we drop specified table from DB
        USHORT randomNum;   // either set by default or by user, this is 
                            // the item number to return
};

#endif

