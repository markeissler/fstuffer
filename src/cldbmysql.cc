/******************************************************************************
 *
 * This file is part of fstuffer.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           cldbmysql.cc
 *
 *  Description:    Contains all functionality for interaction with a mySQL
 *                  database 
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *          
 *          $Log$
 *          Revision 1.3  2000/11/13 19:24:04  mark
 *          Updated to support new error messages with debug code
 *
 *          Revision 1.2  2000/11/11 01:01:33  mark
 *          dbrepair() implemented incorrectly, well we don't have support for query's yet, in dbquery_link() so I commented the section out and replaced error call to fatal. Addresses -Q seg fault issue from 11/10/00.
 *
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include <stdio.h>  // for sprintf()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <mysql.h>
#include <mysqld_error.h>
#include "common.h"
#include <mutil/util_error.h>
#include <mutil/util_prompt.h>
#include <mutil/util_string.h>
#include "cldbmysql.h"

const USHORT k_MAX_mysql_QUERY_LEN = (1024 * 10);

/* clDBMysql::clDBMysql() CONSTRUCTOR
 *
 * Des: Initialize internal variables/arrays then call init() with a reference
 * to the comOpts object passed to us by reference.
 *
 * Rec: -reference to a comOpts object containing parsed command line options.
 *
 * Ret: Nothing.
 *
 * Chg: Set various values in our private data.
 *
 */
clDBMysql::clDBMysql(const clComOpts &r_myComOpts): 
    dbHost(0),
    dbPort(0),
    dbName(0),
    tblName(0),
    user(0),
    pass(0),
    conn(0)
{
    
    init(r_myComOpts);

    return;
}


/* clDBMysql::~clDBMysql() DESTRUCTOR
 *
 * Des: --
 *
 * Rec: Nothing.
 *
 * Ret: Nothing.
 *
 * Chg: Frees allocated memory where necessary.
 *
 */
clDBMysql::~clDBMysql() {
    delete dbHost;
    dbHost = 0;
    delete dbPort;
    dbPort = 0;
    delete dbName;
    dbName = 0;
    delete tblName;
    tblName = 0;
    delete user;
    user = 0;
    delete pass;
    pass = 0;
}


/* clDBMysql::init()
 *
 * Des: <Description>
 *
 * Rec: Copy strings values specified on command line into appropriate
 * string values in our private data. The strings have already been 
 * parsed by the comOpts object which we can refer to to extract the
 * strings and pass ptrs to the set* functions.
 *
 * Ret: Nothing.
 *
 * Chg: Memory is freed/allocated and data is copied as required by the 
 * various set* functions called in this function.
 *
 */
void clDBMysql::init(const clComOpts &r_myComOpts) {

    setDBHost(r_myComOpts.getHost());
    setDBPort(r_myComOpts.getPort());
    setDBName(r_myComOpts.getDName());
    setDBTName(r_myComOpts.getTName());
    setDBUser(r_myComOpts.getUser());
    setDBPass(r_myComOpts.getPass());
}


/* clDBMysql::setDBHost()
 *
 * Des: Free previously allocated memory pointed to by dbHost ptr variable
 * and then allocate new memory of appropriate size for the dbHost string
 * passed on command line. Copy new dbHost value into the allocated memory.
 *
 * Rec: -ptr to a string containing dbHost.
 *
 * Ret: -ptr to private dbHost string.
 *
 * Chg: -memory pointed to by dbHost ptr variable.
 *
 */
char *clDBMysql::setDBHost(const char * const myDBHost) {
    int len = strlen(myDBHost);
    
    if(dbHost != 0)
        delete dbHost;
    
    dbHost = new char[len + 1];
    if(dbHost == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::setDBHost", __LINE__, 
                k_ue_memAlloc);
    strncpy(dbHost, myDBHost, len + 1);
    
    return(dbHost); 
}


/* clDBMysql::setDBPort()
 *
 * Des: Free previously allocated memory pointed to by dbPort ptr variable
 * and then allocate new memory of appropriate size for the dbPort string
 * passed on command line. Copy new dbPort value into the allocated memory.
 *
 * Rec: -ptr to a string containing dbPort.
 *
 * Ret: -ptr to private dbPort string.
 *
 * Chg: -memory pointed to by dbPort ptr variable.
 *
 */
char *clDBMysql::setDBPort(const char * const myDBPort) {
    int len = strlen(myDBPort);
    
    if(dbPort != 0)
        delete dbPort;
        
    dbPort = new char[len + 1];
    if(dbPort == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::setDBPort", __LINE__, 
            k_ue_memAlloc);
    strncpy(dbPort, myDBPort, len + 1);
    
    return(dbPort); 
}


/* clDBMysql::setDBName()
 *
 * Des: Free previously allocated memory pointed to by dbName ptr variable
 * and then allocate new memory of appropriate size for the dbName string
 * passed on command line. Copy new dbName value into the allocated memory.
 *
 * Rec: -ptr to a string containing dbName.
 *
 * Ret: -ptr to private dbName string.
 *
 * Chg: -memory pointed to by dbName ptr variable.
 *
 */
char *clDBMysql::setDBName(const char * const myDBName) {
    int len = strlen(myDBName);
    
    if(dbName != 0)
        delete dbName;
    
    dbName = new char[len + 1];
    if(dbName == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::setDBName", __LINE__, 
            k_ue_memAlloc);
    strncpy(dbName, myDBName, len + 1);
    
    return(dbName);
}


/* clDBMysql::setDBTName()
 *
 * Des: Free previously allocated memory pointed to by tblName ptr variable
 * and then allocate new memory of appropriate size for the tblName string
 * passed on command line. Copy new tblName value into the allocated memory.
 *
 * Rec: -ptr to a string containing tblName.
 *
 * Ret: -ptr to private tblName string.
 *
 * Chg: -memory pointed to by tblName ptr variable.
 *
 */
char *clDBMysql::setDBTName(const char * const myDBTName) {
    int len = strlen(myDBTName);
    
    if(tblName != 0)
        delete tblName;
        
    tblName = new char[len + 1];
    if(tblName == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::setDBTName", __LINE__, 
                k_ue_memAlloc);
    strncpy(tblName, myDBTName, len + 1);
    
    return(tblName);
}


/* clDBMysql::setDBUser()
 *
 * Des: Free previously allocated memory pointed to by user ptr variable
 * and then allocate new memory of appropriate size for the user string
 * passed on command line. Copy new user value into the allocated memory.
 *
 * Rec: -ptr to a string containing user.
 *
 * Ret: -ptr to private user string.
 *
 * Chg: -memory pointed to by user ptr variable.
 *
 */
char *clDBMysql::setDBUser(const char * const myDBUser) {
    int len = strlen(myDBUser);
    
    if(user != 0)
        delete user;
        
    user = new char[len + 1];
    if(user == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::setDBUser", __LINE__, 
                k_ue_memAlloc);
    strncpy(user, myDBUser, len + 1);
    
    return(user);
}


/* clDBMysql::setDBPass()
 *
 * Des: Free previously allocated memory pointed to by pass ptr variable
 * and then allocate new memory of appropriate size for the pass string
 * passed on command line. Copy new pass value into the allocated memory.
 *
 * Rec: -ptr to a string containing pass.
 *
 * Ret: -ptr to private pass string.
 *
 * Chg: -memory pointed to by pass ptr variable.
 *
 */
char *clDBMysql::setDBPass(const char * const myDBPass) {
    int len = strlen(myDBPass);
    
    if(pass != 0)
        delete pass;
        
    pass = new char[len + 1];
    if(pass == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::setDBPass", __LINE__, 
                k_ue_memAlloc);
    strncpy(pass, myDBPass, len + 1);
    
    return(pass);
}


/* clDBMysql::dbinit()
 *
 * Des: Call mysql_init() to initialize the conn struct and stuff ptr
 * into our private conn struct ptr variable.
 *
 * Rec: Nothing.
 *
 * Ret: -bool indicating success (false) or failure (true).
 *
 * Chg: -memory pointed to by conn struct ptr variable.
 *
 */
bool clDBMysql::dbinit() {
    bool myErr = false;
    
    if((conn = mysql_init(NULL)) == NULL)
        myErr = true;
        
    return(myErr);
}

/* clDBMysql::dbopen()
 *
 * Des: Open connection to mysql db using the conn struct pointed to
 * by our private conn struct ptr variable along with the various host,
 * dbName, user, pass, etc., values passed on the command line.
 *
 * NOTE: A call to dbinit() is required before calling this function as
 * the private conn struct must be initialized first!
 *
 * Rec: Nothing.
 *
 * Ret: -bool indicating success (false) or failure (true).
 *
 * Chg: Nothing.
 *
 */
bool clDBMysql::dbopen() {
    bool myErr = false;
    
    if(mysql_real_connect(
        conn,
        dbHost,
        user,
        pass,
        dbName,
        atoi(dbPort),
        NULL,
        0) == NULL) {
            myErr = true;
        }
    
    // if connect failed, return reason for failure
    if(myErr == true) {
        errorCMsg(gpOut, k_ue_warn, __FILE__, "clDBMysql::dbopen", __LINE__, 
            mysql_error(conn), "");
    }
        
    return(myErr);
}


/* clDBMysql::dbclose()
 *
 * Des: Close the connection to the mysql db referenced by the conn struct
 * pointed to by the private conn struct ptr variable.
 *
 * Rec: Nothing.
 *
 * Ret: Nothing.
 *
 * Chg: Closes connection to mysql db. dbopen() would have to be called to
 * re-establish connection.
 *
 */
void clDBMysql::dbclose() {

    mysql_close(conn);
}


/* clDBMysql::dbcreateTable()
 *
 * Des: Create a table in the current database with the current table name.
 * By "current" I mean the values contained in our private data.
 *
 * Rec: Nothing.
 *
 * Ret: -bool indicating success (false) or failure (true).
 *
 * Chg: Changes db in that upon success we will have created a new table.
 *
 */
bool clDBMysql::dbcreateTable() {
    bool myErr = false;
    
    char *query = new char[k_MAX_mysql_QUERY_LEN];
    if(query == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbcreateTable", __LINE__, 
                k_ue_memAlloc);

    sprintf(query, 
        "CREATE TABLE %s (
            body TEXT, 
            body_len INT NOT NULL, 
            auth VARCHAR(255), 
            ref VARCHAR(255));", 
            tblName);

    if(mysql_query(conn, query) != 0) {
        errorCMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbcreateTable", __LINE__, 
                mysql_error(conn), "");
        myErr = true;
    }

    return(myErr);
}


/* clDBMysql::dbdropTable()
 *
 * Des: Drop a table in the current database with the current table name.
 * By "current" I mean the values contained in our private data.
 *
 * Rec: Nothing.
 *
 * Ret: -bool indicating success (false) or failure (true).
 *
 * Chg: Changes db in that upon success we will have dropped a table.
 *
 */
bool clDBMysql::dbdropTable() {
    bool myErr = false;

    char *query = new char[k_MAX_mysql_QUERY_LEN];
    if(query == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbdropTable", __LINE__, 
                k_ue_memAlloc);
    
    sprintf(query, 
        "DROP TABLE %s;", tblName);
        
    if(mysql_query(conn, query) != 0) {
        errorCMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbdropTable", __LINE__, 
                mysql_error(conn), "");
        myErr = true;
    }

    return(myErr);  
}


/* clDBMysql::dbpromptDropTable()
 *
 * Des: Wraps the dbdropTable() function within a prompt to confirm action
 * before proceeding.
 *
 * Rec: -ptr to FILE struct for input stream (typically stdin)
 *      -ptr to FILE struct for output stream (typically stdout).
 *
 * Ret: Nothing.
 *
 * Chg: Nothing.
 *
 */
bool clDBMysql::dbpromptDropTable(FILE *istream, FILE *ostream) {
    bool myErr = false;
    char c;
    
    char *aStr = new char;  // this will be resized by promptCList()
    if(aStr == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbpromptDropTable", __LINE__, 
                k_ue_memAlloc);
    aStr = '\0';

    // drop table after prompting
    fprintf(ostream, "Are you sure you want to drop \"%s\" from \"%s\" database? ",
        getDBTName(), getDBName());

    c = promptCList(istream, ostream, &aStr, "(Y/N): ", 4, "Y", "N", "yes", "no", "");
    
    switch(c) {
        case 'Y':
            myErr = dbdropTable();
            break;
            
        case 'N':
        default:
            myErr = true;
            break;
    }
    
    return(myErr);
}


/* clDBMysql::dbinsert()
 *
 * Des: Insert data in clFItem (passed to us by reference) into the current
 * table in the current database.
 * By "current" I mean the values contained in our private data.
 *
 * Rec: -reference to clFItem.
 *
 * Ret: -bool indicating success (false) or failure (true).
 *
 * Chg: Changes db in that upon success we will have added data from the 
 * clFItem into the current table.
 *
 */
bool clDBMysql::dbinsert(const clFItem &r_myFItem) {
    bool myErr = false;
    eString *tempBody, *tempAuth, *tempRef;
        
    if((tempBody = str_eStr(r_myFItem.getBody(), strlen(r_myFItem.getBody()))) == NULL)
                errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbinsert", __LINE__, 
                        k_ue_memAlloc);

    if((tempAuth = str_eStr(r_myFItem.getAuth(), strlen(r_myFItem.getAuth()))) == NULL)
                errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbinsert", __LINE__, 
                        k_ue_memAlloc);
    
    if((tempRef = str_eStr(r_myFItem.getRef(), strlen(r_myFItem.getRef()))) == NULL)
                errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbinsert", __LINE__, 
                        k_ue_memAlloc);
 
    char *query = new char[k_MAX_mysql_QUERY_LEN];
    if(query == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbinsert", __LINE__, k_ue_memAlloc);
    
    sprintf(query, 
        "INSERT INTO %s (body, body_len, auth, ref) VALUES('%s', '%d', '%s', '%s');",
            tblName,
            tempBody->str,
            tempBody->len,
            tempAuth->str,
            tempRef->str);

    /* Call mysql_query() with the above query, if it fails
     * then we call dbrepair() to try and fix the problem by
     * creating the table or whatever, then we make another
     * call to dbinsert() to add the item.
     *
     * The dbinsert() function, that is, this function, is 
     * called recursively until it returns an error because
     * dbrepair() has found a problem it can't fix or we 
     * succeed in adding the item.
     */
    if(mysql_query(conn, query) != 0) {
        // see if this was a missing table
        if((myErr = dbrepair(mysql_errno(conn))) == true)
            errorCMsg(gpOut, k_ue_warn, __FILE__, "clDBMysql::dbinsert", __LINE__, 
                    mysql_error(conn), "");
        else
            // call dbinsert() recursively
            myErr = dbinsert(r_myFItem);
    }

    free_eStr(tempBody);
    free_eStr(tempAuth);
    free_eStr(tempRef);
    
    return(myErr);
}


/* clDBMysql::dbinsert_link()
 *
 * Des: Takes a reference to a clfitem linked list node object and 
 * iterates through the linked list, inserting each item into the 
 * database.
 *
 * Rec: reference to a clftiem linked list node (clFItemNode) which is, 
 * typically, the head pointer.
 *
 * Ret: Bool value indicating success or failure.
 *
 * Chg: Adds records to the DB.
 *
 */
bool clDBMysql::dbinsert_link(const clFItemNode &r_myFItemNodeHead) {
    bool myErr = false;
    
    const clFItemNode *tempHead = &r_myFItemNodeHead;
        
    do {
        // insert the item       
        myErr = dbinsert(*(tempHead->getclFItem()));
            
    } while((myErr == false) && ((tempHead = tempHead->getNextNode()) != 0));
    
    return(myErr);

}


/* clDBMysql::dbquery_link()
 *
 * Des: Extracts entries from a fortune table in a mysql db and stuffs
 * each record into a linked list of clFItems.
 *
 * Rec: Address to a clFItemNode variable (passed as a ptr to a ptr also
 * known as a "handle."
 *
 * Ret: Bool value indicating success or failure.
 *
 * Chg: Allocates memory for linked list and changes address pointed
 * to by clFItemNode ptr variable passed to us.
 *
 */
bool clDBMysql::dbquery_link(clFItemNode **h_myFItemNodeHead) {
    bool myErr = false;
    int entries = 0;

    clFItemNode *listHead = 0;
    clFItemNode *newNode = 0;
    clFItem *newclFItem = 0;

    MYSQL_RES *result_set = 0;
    MYSQL_ROW row;
    
    char *query = new char[k_MAX_mysql_QUERY_LEN];
    if(query == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbquery_link", __LINE__, 
                k_ue_memAlloc);
    
    sprintf(query, 
        "SELECT body, body_len, auth, ref FROM %s;",
            tblName);

    /* Call mysql_query() with the above query, if it fails
     * then we call dbrepair() to try and fix the problem by
     * creating the table or whatever, then we make another
     * call to dbinsert() to add the item.
     *
     * The dbinsert() function, that is, this function, is 
     * called recursively until it returns an error because
     * dbrepair() has found a problem it can't fix or we 
     * succeed in adding the item.
     */
    if(mysql_query(conn, query) != 0) {
        errorCMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbquery_link", __LINE__, 
                mysql_error(conn), "");

        // see if this was a missing table
        //if((myErr = dbrepair(mysql_errno(conn))) == true)
        //    errorCMsg(k_ue_warn, __FILE__, __LINE__, mysql_error(conn), "");
        //else
            // call dbquery_link() recursively
        //    myErr = dbquery_link(&listHead);
    }

    // code to read rows and stuff data into linked list
    // we only populate listHead if this hasn't been done
    // already...
    
    if(myErr == false && listHead == 0) {
        result_set = mysql_store_result(conn);
        
        while((row = mysql_fetch_row(result_set)) != NULL) {
            // copy data to a clFItemNode in linked list
            newclFItem = new clFItem(row[0], atoi(row[1]), row[2], row[3]);
            if(newclFItem == 0)
                errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbquery_link", __LINE__, 
                        k_ue_memAlloc);

            // inesert the item into the linked list
            if (entries == 0) {
                listHead = new clFItemNode(newclFItem);
                if(listHead == 0)
                    errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbquery_link", __LINE__, 
                            k_ue_memAlloc);
            } else {
                newNode = new clFItemNode(newclFItem);
                if(newNode == 0)
                    errorMsg(gpOut, k_ue_fatal, __FILE__, "clDBMysql::dbquery_link", __LINE__, 
                            k_ue_memAlloc);
        
                listHead->insertNode(newNode);
                
                // increment item counter
                listHead->setListLen(listHead->getListLen() + 1);
            } 

            entries++;
        }
        
        mysql_free_result(result_set);
    }

    *h_myFItemNodeHead = listHead;
        
    return(myErr);
}


/* clDBMysql::dbrepair()
 *
 * Des: Usually called when an error condition is encountered while
 * communicating with the DB. Pass the error code to this routine
 * and we'll try and repair the problem otherwise we'll return false
 * to indicate failure.
 *
 * Rec: USHORT indicating error code from the DB.
 *
 * Ret: Bool value indicating success or failure.
 *
 * Chg: Nothing.
 *
 */
bool clDBMysql::dbrepair(USHORT dbErrCode) {
    bool myErr = false;
    
    switch(dbErrCode) {
        case ER_NO_SUCH_TABLE:          // table doesn't exist, try to make it
            myErr = dbcreateTable();
            break;
    
        default:
            myErr = true;
            break;
    }

    return(myErr);
}

