/******************************************************************************
 *
 * This file is part of fstuffer.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           clfitem.h
 *
 *  Description:    Contains functionality for further parsing fortune items
 *                  supplied by routines in clffile.cc into body, body length,
 *                  author and reference components; provides data output
 *                  routines for stepping through a linked list of clfitems
 *                  contained in clfitemnodes.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include "common.h"

#ifndef __CL_FITEM__
#define __CL_FITEM__

class clFItem {
    public:
        clFItem(USHORT inFTextLen, char * const inFText);
        clFItem(const char * const inBody, USHORT inBodylen, 
            const char * const inAuth, const char * const inRef);
        ~clFItem();
        
        char *getBody() const { return body; }
        USHORT getBodyLen() const { return bodylen; }
        char *getAuth() const { return auth; }
        char *getRef() const { return ref; }
        void init(USHORT inFTextLen, char * const inFText);
        char *set(USHORT inFTextLen, char * const inFText);

    private:
        char *body;
        USHORT bodylen;
        char *auth;
        char *ref;

};


class clFItemNode {
    public:
        clFItemNode(clFItem *newclFItem);
        ~clFItemNode();
        
        clFItemNode *getNextNode() const;
        clFItem *getclFItem() const;
        USHORT getListLen() const { return myListLen; }
        USHORT getListIndex() const { return s_myListIndex; }
        clFItemNode *getCachedNode() const;
        void setNextNode(clFItemNode *nextNode);
        void setListLen(USHORT newListLen) { myListLen = newListLen; }
        void setListIndex(USHORT newListIndex) { s_myListIndex = newListIndex; }
        void setCachedNode(clFItemNode *cachedNode);
        void insertNode(clFItemNode *newNode);
        void show_link(USHORT maxItemLen = 0, bool verbosity = false, USHORT randomNum = 0);
    
    private:
        clFItem *myclFItem;
        clFItemNode *myNextNode;
        USHORT myListLen;   // incremented (each time we insert a node) by
                            // calling setListLen(getListLen() + 1)
        static USHORT s_myListIndex;    // current item entry in linked list
        static clFItemNode *s_myCachedNode;
};

#endif

