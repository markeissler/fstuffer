/******************************************************************************
 *
 * This file is part of fstuffer.
 *
 * Copyright (C) 2000 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           clfitem.cc
 *
 *  Description:    Contains functionality for further parsing fortune items
 *                  supplied by routines in clffile.cc into body, body length,
 *                  author and reference components; provides data output
 *                  routines for stepping through a linked list of clfitems
 *                  contained in clfitemnodes.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.2  2000/11/11 00:16:22  mark
 *          Fixed item parsing in set() when items are missing a % item delimiter. See changelog.
 *
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include <stdio.h>
#include <fstream>
#include <limits.h>
#include <iomanip.h>
#include <string.h> // for memmove
#include "common.h"
#include <mutil/util_error.h>
#include <mutil/util_string.h>
#include "clfitem.h"


/* clFItem::clFItem() CONSTRUCTOR
 *
 * Des: Calls init() to initialize private variables/arrays, the parses
 * and copies data from inBody char array. Typically used when parsing
 * the fortune file (a situation in which the file is parsed into items
 * which must be further parsed into their component fields: body, body
 * length, author, reference).
 *
 * Rec: -USHORT indicating length of buffer
 *      -ptr to char buffer containing unparsed item text
 *
 * Ret: Nothing.
 *
 * Chg: Set various values in our private data.
 *
 */
clFItem::clFItem(USHORT inFTextLen, char * const inFText) {
    
    init(inFTextLen, inFText);

    return;
}


/* clFItem::clFItem() CONSTRUCTOR
 *
 * Des: Initializes private variables/arrays with data contained in the
 * arrays passed (by reference) to us. Typically used when retrieving 
 * data from a database in which fields require no further parsing.
 *
 * Rec: -ptr to char array containing parsed body text
 *      -USHORT indicating body text length (inBody length)
 *      -ptr to char array containing parsed author string
 *      -ptr to char array containing parsed reference string
 *
 * Ret: Nothing.
 *
 * Chg: Memory allocated for private variables/arrays and data copied 
 * from inBody, inBodyLen, inAuth, inRef, to private containers.
 *
 */
clFItem::clFItem(const char * const inBody, USHORT inBodylen, 
            const char * const inAuth, const char * const inRef) {
            
    body = new char[strlen(inBody) + 1];
    if(body == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clFItem::clFItem", __LINE__, k_ue_memAlloc);
    strcpy(body, inBody);
    
    auth = new char[strlen(inAuth) + 1];
    if(auth == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clFItem::clFItem", __LINE__, k_ue_memAlloc);
    strcpy(auth, inAuth);
    
    ref = new char[strlen(inRef) + 1];
    if(ref == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clFItem::clFItem", __LINE__, k_ue_memAlloc);
    strcpy(ref, inRef);
    
    bodylen = inBodylen;
    
    return;
}


/* clFItem::~clFItem() DESTRUCTOR
 *
 * Des: Frees memory allocated to various private data arrays.
 *
 * Rec: Nothing.
 *
 * Ret: Nothing.
 *
 * Chg: Frees allocated memory where necessary.
 *
 */
 clFItem::~clFItem() {
    delete body;
    body = 0;
    delete auth;
    auth = 0;
    delete ref;
    ref = 0;
}


/* clFItem::init()
 *
 * Des: Initializes private data arrays to null terminated zero length 
 * values. Then calls set() which will parse the char buffer pointed to
 * by inFText prior to copying the data into our private data arrays.
 *
 * Rec: -USHORT indicating length of buffer
 *      -ptr to char buffer containing unparsed item text
 *
 * Ret: Nothing.
 *
 * Chg: Memory allocated for private variables/arrays and data copied 
 * from inBody, inBodyLen, inAuth, inRef, to private containers via the
 * set() function.
 *
 */
void clFItem::init(USHORT inFTextLen, char * const inFText) {
    body = new char;
    if(body == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clFItem::init", __LINE__, 
            k_ue_memAlloc);
    *body = '\0';
    
    auth = new char;
    if(auth == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clFItem::init", __LINE__, 
            k_ue_memAlloc);
    *auth = '\0';
    
    ref = new char;
    if(ref == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clFItem::init", __LINE__, 
            k_ue_memAlloc);
    *ref = '\0';

    set(inFTextLen, inFText);
}


/* clFItem::set()
 *
 * Des: Reads the char buffer (whose ptr is passed to us along with the
 * buffer's length) and further parses the fortune item into its body,
 * author (auth) and reference (ref) components. This information, along
 * with the body length, is used to initialize the clFItem.
 *
 * NOTE on parsing: The way we currently know to stop parsing is by
 * looking for a '\0' char. The only place we'll find a '\0' is where we
 * inserted one in place of the '%' that previously ended the item. 
 *
 * Rec: USHORT containing length of char buffer,
 *      constant ptr to an array of chars (the buffer) containg text of a
 * fortune item.
 *
 * Ret: -ptr to private data char array containing copied body text.
 *
 * Chg: Modifies the values stored in our private variables; Allocates
 * memory for arrays as applicable (body, auth, ref).
 *
 */
char *clFItem::set(USHORT inFTextLen, char * const inFText) {
    char *tempStr;
    char *p;
    int i = 1;
    int bodyFlag = 1, authFlag = 0, refFlag = 0;

    tempStr = new char[inFTextLen];
    if(tempStr == 0)
        errorMsg(gpOut, k_ue_fatal, __FILE__, "clFItem::set", __LINE__, k_ue_memAlloc);

    for(p = inFText; *p; p++) {
        if(bodyFlag == 1 || authFlag == 1 || refFlag == 1 || refFlag == 2)
            *(tempStr + (i - 1)) = *p;

        /* Author section starts with "\t--" and end with
         * a "," (typically). Note: as of 0.9.1 we only
         * recognize this delimiter if we're currently in
         * the body section (bodyFlag == 1). There are
         * some screwed up fortune files (like "work")
         * which omit the % item delimiter in some cases
         * and that totally screws us. We end up dumping
         * the following item.
         */
        if(bodyFlag == 1 && *p == '-' && *(p + 1) == '-' && *(p - 1) == '\t') {
            bodyFlag = 0;
            authFlag = 1;
            
            // closeout body section
            *(tempStr + (i - 1)) = '\0';
            str_copyStr(&body, tempStr);
            bodylen = (i - 1);
            i = 0;
            p++;
            
            // cout << body << endl;
        }
        
        /* If we're in the Body section and the next char
         * is a '\0' then we're going to hit item end at
         * next time through...
         *
         * There is no auth section.
         * There is no ref section.
         *
         * Replace the '\n' that will end this section
         * with a '\0'.
         *
         */
        if(bodyFlag == 1 && *(p + 1) == '\0') {
            bodyFlag = 0;
            
            // closeout body section
            *(tempStr + (i - 1)) = '\0';
            str_copyStr(&body, tempStr);
            bodylen = (i - 1);
            i = 0;
            
            // cout << body << endl;
        }
        
        /* If we're in the Auth section and hit
         * a "," then we're into the Ref section.
         */ 
        if (authFlag == 1 && *p == ',') {
            authFlag = 0;
            refFlag = 1;
            
            // closeout auth section
            *(tempStr + (i - 1)) = '\0';
            str_copyStr(&auth, tempStr);
            str_shrinkStr(&auth);
            i = 0;
            
            // cout << auth << endl;
        }
        
        /* If we're in the Auth seciton and hit a "["
         * then we're into a bracketed Ref section.
         */
        if (authFlag == 1 && *p == '[') {
            authFlag = 0;
            refFlag = 2;
            
            // closeout auth section
            *(tempStr + (i - 1)) = '\0';
            str_copyStr(&auth, tempStr);
            str_shrinkStr(&auth);
            i = 0;
            
            // preserve leading bracket
            p--;
            
            // cout << auth << endl;
        }
        
        /* If we're in the Auth section and the next char
         * is a '\0' then we're going to hit item end at
         * next time through...
         *
         * There is no ref section.
         *
         * Replace the '\n' that will end this section
         * with a '\0'.
         *
         */
        if (authFlag == 1 && *(p + 1) == '\0') {
            authFlag = 0;
            
            // closeout auth section
            *(tempStr + (i - 1)) = '\0';
            str_copyStr(&auth, tempStr);
            str_shrinkStr(&auth);
            i = 0;
        
            // cout << auth << endl;
        }
        
         
         /* As of 0.9.1...
          *
          * If we're in the Auth section and we hit two
          * successive newlines ('\n') then we will assume
          * that we've reached item end but the '%' item
          * delimiter character is missing.
          *
          * There is no ref section.
          *
          * Replace the first '\n' with a '\0' and skip
          * the rest (meaning, the next item).
          *
          */
         if (authFlag == 1 && (*p == '\n' && *(p + 1) == '\n')) {
            authFlag = 0;
            
            // closeout auth section
            *(tempStr + (i - 1)) = '\0';
            str_copyStr(&auth, tempStr);
            str_shrinkStr(&auth);
            i = 0;
        }
      
        /* If we're in the Ref section and the next char
         * is a '\0' then we're going to hit item end at
         * next time through...
         *
         * Replace the '\n' that will end this section
         * with a '\0'.
         *
         */
        if (refFlag == 1 && *(p + 1) == '\0') {
            refFlag = 0;
            
            // closeout ref section
            *(tempStr + (i - 1)) = '\0';
            str_copyStr(&ref, tempStr);
            str_shrinkStr(&ref);
            str_compressStr(&ref);
            i = 0;
            
            // cout << ref << endl;
        }
        
        
        /* If we're in the Ref section and the next char
         * is a '\0' then we're going to hit item end at
         * next time through...
         *
         * Replace the '\n' that will end this section
         * with a '\0'.
         *
         */
        if(refFlag == 2 && *(p + 1) == '\0') {
            refFlag = 0;
            
            // closeout ref section
            *(tempStr + (i - 1)) = '\0';
            str_copyStr(&ref, tempStr);
            str_shrinkStr(&ref);
            str_compressStr(&ref);
            i = 0;
            
            // cout << ref << endl;
        }
        

        i++;    

    }
    
    delete tempStr;

    return(body);
}



/* clFItemNode::clFItemNode() CONSTRUCTOR
 *
 * Des: Sets the private data variables for the first item in a linked
 * list of clFItem objects. 
 *
 * Rec: -ptr to a clFItem
 *
 * Ret: Nothing.
 *
 * Chg: Set various values in our private data.
 *
 */
clFItemNode::clFItemNode(clFItem *newclFItem):
    myclFItem(newclFItem),
    myNextNode(0),
    myListLen(1)
{}


/* clFItemNode::~clFItemNode() DESTRUCTOR
 *
 * Des: Frees memory allocated to various private data arrays.
 *
 * Rec: Nothing.
 *
 * Ret: Nothing.
 *
 * Chg: Frees allocated memory where necessary.
 *
 */
clFItemNode::~clFItemNode() {
    delete myclFItem;
    myclFItem = 0;
    delete myNextNode;
    myNextNode = 0;
}


/* clFItemNode::getNextNode()
 *
 * Des: Returns ptr to the next clFItemNode as contained in private data
 * member myNextNode ptr variable in this object.
 *
 * Rec: Nothing.
 *
 * Ret: -ptr to next clFItemNode in linked list.
 *
 * Chg: Nothing.
 *
 */
clFItemNode *clFItemNode::getNextNode() const {

    return(myNextNode);
}


/* clFItemNode::getclFItem()
 *
 * Des: Returns ptr to the clFItem contained in private data member myclFItem
 * in this node.
 *
 * Rec: Nothing.
 *
 * Ret: -ptr to clFItem in this node in linked list.
 *
 * Chg: Nothing.
 *
 */
clFItem *clFItemNode::getclFItem() const {

    return(myclFItem);
}


/* clFItemNode::getCachedNode()
 *
 * Des: Returns ptr to a clFItem whose ptr is contained in the static cached 
 * node class variable. This is the last previous item retrieved in the linked
 * list (which can be affected by max item length parameters). 
 *
 * Rec: Nothing.
 *
 * Ret: -ptr to clFItem in the static cached node class variable.
 *
 * Chg: Nothing.
 *
 */
clFItemNode *clFItemNode::getCachedNode() const {

    return(s_myCachedNode);
}


/* clFItemNode::setNextNode()
 *
 * Des: Sets the private data member myNextNode to point to the next node in
 * the linked list.
 *
 * Rec: -ptr to next clFItem node in the linked list.
 *
 * Ret: Nothing.
 *
 * Chg: <memory changed> or Nothing.
 *
 */
void clFItemNode::setNextNode(clFItemNode *nextNode) {

    myNextNode = nextNode;
}

void clFItemNode::setCachedNode(clFItemNode *cachedNode) {

    s_myCachedNode = cachedNode;
}

/* clFItemNode::insertNode()
 *
 * Des: This function will traverse the linked by calling itself 
 * recursively and chcking the nextNode value. If nextNode has a
 * pointer to a Node, then we call that Node's insertNode()
 * function and continue doing that until we find an empty next
 * node where we can insert a new Node. In essence, we need to get
 * to the end of the linked list. There is probably a better way
 * that we can do this.
 *
 * Rec: -ptr to a Node object
 *
 * Ret: Nothing.
 *
 * Chg: Adds a node to the linked list.
 */
void clFItemNode::insertNode(clFItemNode *newNode) {

    if(myNextNode == 0) {   // no next node, insert in this record
        setNextNode(newNode);
    } else {                // call insertNode on the next node
        myNextNode->insertNode(newNode);
    }
}

/* show_link()
 *
 * Des: Iterate through a linked list of clFItemNode objects, exracting the
 * clFItem in each and then sending the extracted data to stdout. Output is
 * modified by three parameters: maximum item length (maxItemLen), verbosity,
 * and random item (randomNum).
 *
 * If maxItemLen has been set, then we don't output items longer than the 
 * length (in bytes) specified.
 *
 * If verbosity has been set, then we show extra meta information.
 *
 * If randomNum has been set, then we output only the item that corresponds
 * exactly to the randomNum or the last matching item occurring prior to the
 * randomNum. That latter point is important, because it is conceivable that
 * the randomNum specified is higher than the last item that matches the 
 * maxItemLen criteria. Of course, if not item matches the maxItemLen, then 
 * we output nothing.
 *
 * Rec: USHORT indicating maximum item length in bytes (applies to body),
 *      bool value indicating verbosity,
 *      USHORT indicating random item to be displayed (specified by user on
 * the command line or generated by rand() in the main() function.
 *
 * Ret: Nothing.
 *
 * Chg: Nothing.
 */
void clFItemNode::show_link(USHORT maxItemLen, bool verbosity, USHORT randomNum) {
    setListIndex(getListIndex() + 1);

    if(maxItemLen == 0 || (maxItemLen > 0 && myclFItem->getBodyLen() <= maxItemLen)) {
        
        /*
         * Cache all items if random is off, otherwise cache only
         * items that are found up to the randomNum in the index.
         *
         */
        if(randomNum == 0 || (randomNum != 0 && getListIndex() <= randomNum))
            setCachedNode(this);
            
    }

        
    /*
     * Print when? We always print the cached item. But we only print
     * when random is off or getListIndex() equals randomNum.
     *
     * Also, we don't print if the cache is empty. Otherwise we'll seg
     * fault. And we don't like that.
     *
     */
    if((randomNum == 0 || (getListIndex() == randomNum)) && getCachedNode() != 0) {
        if(verbosity == false) {
            // no verbosity
            cout << getCachedNode()->getclFItem()->getBody() << endl;

            // output author if there is one
            if(strlen(getCachedNode()->getclFItem()->getAuth()) > 0)
                cout << "\t" << getCachedNode()->getclFItem()->getAuth() << endl;

            // output reference if there is one
            if(strlen(getCachedNode()->getclFItem()->getRef()) > 0)
                cout << "\t(" << getCachedNode()->getclFItem()->getRef() << ")" << endl;
        
            // output a separator if random is off
            if(randomNum == 0)
                cout << endl << "----\n" << endl;

        } else {
            // verbosity
            
            cout << ">>>> BEG ITEM #" << getListIndex() <<": " 
            << getCachedNode()->getclFItem()->getBodyLen() << " bytes";
        
            if(maxItemLen == 0)
                cout << "  MAX ITEM: --" << " bytes";
            else
                cout << "  MAX ITEM: " << maxItemLen << " bytes";
                
            if(randomNum == 0)
                cout << "  RAN ITEM: --" << endl;
            else
                cout << "  RAN ITEM: " << randomNum << endl;
                
            cout << getCachedNode()->getclFItem()->getBody() << endl;
        
            cout << "Author: " << getCachedNode()->getclFItem()->getAuth() << endl;
            cout << "Reference: " << getCachedNode()->getclFItem()->getRef() << endl;
                    
            cout << "<<<< END ITEM" << endl << endl;

        } // end verbosity
        
    }
    
    
    /*
     * Only cycle through every item in the linked list if randomNum
     * has been set to 0. Otherwise, only go as far as randomNum.
     *
     */
    if(myNextNode != 0 && (randomNum == 0 || (randomNum > 0 && getListIndex() <= randomNum)))
        myNextNode->show_link(maxItemLen, verbosity, randomNum);

}

// define and initialize static variables
USHORT clFItemNode::s_myListIndex = 0;
clFItemNode *clFItemNode::s_myCachedNode = 0;
