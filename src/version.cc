/******************************************************************************
 *
 * This file is part of fstuffer.
 *
 * Copyright (C) 2000,2001 Mark Eissler <eisslerm@mac.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING in this source file
 * distribution's root directory. If not, write to the Free Software 
 * Foundation, Inc., at the following address:
 *
 *      Free Software Foundation, Inc.
 *      59 Temple Place - Suite 330, 
 *      Boston, MA  02111-1307, USA.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *  File:           version.cc
 *
 *  Description:    Contains functionality to output basic version info.
 *
 *  Author(s):      Mark Eissler <eisslerm@mac.com>
 *
 *  Source:         Started anew.
 *
 *  Notes:          
 *
 *  Change History:
 *          $Id$
 *          $Name$
 *
 *          $Log$
 *          Revision 1.3  2001/12/18 23:55:15  mark
 *          Updated copyright to 2001
 *
 *          Revision 1.2  2000/11/13 19:24:49  mark
 *          Updated to support new error messages with debug code
 *
 *          Revision 1.1.1.1  2000/11/09 23:54:57  mark
 *          Import of fstuffer sources
 *
 *          
 *  
 *****************************************************************************/
#include <stdio.h>
#include <string.h>
#include "common.h"


/* version()
 *
 * Des: Prints version info then returns.
 *
 * Rec: Nothing.
 *
 * Ret: Nothing.
 *
 * Chg: Nothing.
 *
 */
void version(void) {
    
    // Overview
    fprintf(gpOut, "%s %s\n", kDefPackage, kDefVersion);
    fprintf(gpOut, "\n");
    fprintf(gpOut, "Copyright (C) 2000,2001 Mark Eissler, Mixtur, Inc.\n");
    fprintf(gpOut, "This is free software; see the source for copying conditions.  There is NO\n");
    fprintf(gpOut, "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n");
    fprintf(gpOut, "\n");
    fprintf(gpOut, "You may freely redistribute copies of this software under the terms of the\n");
    fprintf(gpOut, "GNU General Public License. See COPYING in the source distribution.\n");
    fprintf(gpOut, "\n");

    return;
}
